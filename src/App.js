import React, { Component } from 'react';
import ApolloProvider from "react-apollo/ApolloProvider";
import ApolloClient from "apollo-boost";
import {BrowserRouter as Router, Route} from "react-router-dom";

import CommitList from "./components/commit-list/CommitList";
import Commit from "./components/commit/Commit";
import logo from "./logo.png";
import './App.css';

const client = new ApolloClient ({
    'uri': process.env.REACT_APP_GRAPHQL_URL
});

class App extends Component {
  render() {
    return (
        <ApolloProvider client={client}>
            <Router>
                <div className="App">
                    <img className="App-logo" src={logo} alt="logo" />
                    <Route exact={true} path="/" component={CommitList} />
                    <Route path="/commit/:sha" component={Commit} />
                </div>
            </Router>
        </ApolloProvider>
    );
  }
}

export default App;
