import React, { Component } from 'react';
import gql from 'graphql-tag';
import {Query} from 'react-apollo';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import LinearProgress from "@material-ui/core/LinearProgress";
import Paper from "@material-ui/core/Paper";
import {Link} from "react-router-dom";

const COMMIT_LIST_QUERY = gql`
    query CommitListQuery {
      commits {
        id,
        title,
        message,
        author_name
      }
    }
`;

export default class CommitList extends Component {
    render() {
        return (
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Title</TableCell>
                            <TableCell>Message</TableCell>
                            <TableCell>Author Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <Query query={COMMIT_LIST_QUERY}>
                        {
                            ({loading, error, data}) => {
                                if (loading) {
                                    return <TableRow>
                                                <TableCell colSpan={4}>
                                                    <LinearProgress color="primary" />
                                                </TableCell>
                                           </TableRow>;
                                }
                                if (error) {
                                    console.error(error);
                                }
                                return data.commits.map(({id, title, message, author_name}) => (
                                            <TableRow key={id}>
                                                <TableCell>
                                                    <Link to={`/commit/${id}`}> {id}</Link>`
                                                </TableCell>
                                                <TableCell>{title}</TableCell>
                                                <TableCell>{message}</TableCell>
                                                <TableCell>{author_name}</TableCell>
                                            </TableRow>
                                            )
                                        );
                            }
                        }
                        </Query>
                    </TableBody>
                </Table>
            </Paper>
        );
    }
}
