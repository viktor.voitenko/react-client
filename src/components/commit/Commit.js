import React, { Component } from 'react';
import gql from 'graphql-tag';
import {Query} from 'react-apollo';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import LinearProgress from "@material-ui/core/LinearProgress";
import {Button} from "@material-ui/core";
import {Link} from "react-router-dom";
import classNames from 'classnames';
import Moment from "react-moment";
import './Commit.css';


const COMMIT_DETAIL_QUERY = gql`
    query CommitQuery($sha: String!) {
      commit (sha: $sha) {
        title,
        created_at,
        message,
        author_name,
        committer_name,
        committer_email,
        last_pipeline {
          ref,
          sha,
          status
        }
      }
    }
`;

class Commit extends Component {
    render() {
        const sha = this.props.match.params.sha;

        return (
            <Query query={COMMIT_DETAIL_QUERY} variables={{sha}}>
                {
                    ({loading, error, data}) => {
                        if (loading) return <LinearProgress color="primary" />;
                        if (error) console.log(error);
                        const {
                            title,
                            created_at,
                            message,
                            author_name,
                            committer_name,
                            committer_email,
                            last_pipeline: {
                                ref,
                                sha,
                                status
                            }
                        } = data.commit;

                        const isSuccessStatus = status === 'success';

                        return (
                            <Paper>
                                <Grid container justify={"space-around"}>
                                    <Grid>
                                        <Avatar className="avatar">{author_name.charAt(0)}</Avatar>
                                    </Grid>
                                    <Grid item container xs={10}>
                                        <Grid item xs={6} container direction="column">
                                            <Grid item>
                                                <Typography gutterBottom variant="subtitle1">{title}</Typography>
                                                <Typography gutterBottom variant="subtitle2">{message}</Typography>
                                                <Typography gutterBottom variant="subtitle2">
                                                    <Moment format="YYYY-MM-DD HH:MM:SS">{created_at}</Moment>
                                                </Typography>
                                                <Typography color="textSecondary">{committer_name} ({committer_email})</Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="subtitle1">Pipeline:</Typography>
                                            <Typography color="textSecondary" variant="subtitle2">{ref}</Typography>
                                            <Typography color="textSecondary" variant="subtitle2">{sha}</Typography>
                                            <div className={classNames('status', {
                                                'success': isSuccessStatus,
                                                'failed': !isSuccessStatus,
                                            })}>
                                                {status}
                                            </div>
                                        </Grid>
                                    </Grid>
                                    <Grid xs={11}>
                                        <Link className={"backLink"} to='/'>
                                            <Button color={"primary"} variant={"outlined"}>Back</Button>
                                        </Link>
                                    </Grid>
                                </Grid>
                            </Paper>
                        );
                    }
                }
            </Query>
        );
    }
}

export default Commit;
